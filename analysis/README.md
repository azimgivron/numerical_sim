# Towards a simpler model
Let's assume constant thermal conductivity in the cylinder.
```math
\frac{\partial }{\partial r}(r \frac{\partial T}{\partial r}) = -\frac{E_{gen}}{k}r
```
The solution we get from integrating with respect to r two times in a row and using the boundary conditions, gives
```math
T(r) = T_{surface} + \frac{E{gen}}{4k}(r_{surface}^2 - r^2)
```

The optimal constante value to use is the average value k(T) from k(T_surface=T_min) to k(T_center=T_max).
that we can get from the expression k(T) (from https://www.nuclear-power.com/nuclear-engineering/heat-transfer/thermal-conduction/thermal-conductivity/thermal-conductivity-of-uranium-dioxide/).

![k0](figures/_k0.png)

The figure shows how the thermal conductivity evolves and we can use the extreme scenarios k_max and k_min to see the difference in temperature this gives. We know that our solution will be contained between the two curves.

![profile](figures/_profile_korner_cases.png)

In the worst case, the temperature increases to about 1110K. In the best case, with the highest thermal conductivity, the temperature reaches about 1095K. Hence, the difference in temperature reached in both case is very similar. It informs us that the center of the cylinder reaches a temperature between 1095K and 1110K. Note that in the worst case scenario, the melting is not reached. Therefore, it will never be reached.

![k1](figures/_k1.png)

# Accounting for variable thermal conductivity
The solution obtained is aligned with the intuition, as the following graphs show:
![comparison](figures/guesses_vs_truth.png)

Additionally, the final solution lies between the korner cases as illustrated below.
![comparison](figures/T_profiles.png)

It can be seen again that the melting point is not reached. The highest temperature reached at the ceylinder center is about 1096K.


