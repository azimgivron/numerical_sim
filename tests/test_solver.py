import pytest
import numpy as np
from src.solver import boundary_conditions


@pytest.fixture
def boundaries_a() -> np.ndarray:
    return (1173, 0, 2.8)


@pytest.fixture
def boundaries_b() -> np.ndarray:
    return (1073, 100, 3)


@pytest.fixture
def T_surface() -> float:
    return 1073


@pytest.fixture
def k_surface() -> float:
    return 3


def test_boundary_conditions(
    boundaries_a: np.ndarray,
    boundaries_b: np.ndarray,
    T_surface: float,
    k_surface: float,
) -> None:
    sol = boundary_conditions(boundaries_a, boundaries_b, T_surface, k_surface)
    assert (boundaries_b[0] - T_surface) == sol[0]
    assert (boundaries_a[1]) == sol[1]
    assert (boundaries_b[2] - k_surface) == sol[2]
