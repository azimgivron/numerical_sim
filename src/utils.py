"""
Contains generic fonction usefull for different files
"""
import numpy as np
from src.constants import *


def thermal_cond(T: float) -> float:
    """
    Computes the thermal conductivity of uranium dioxyde from the temperature.
    Definition comes from
    https://www.nuclear-power.com/nuclear-engineering/heat-transfer/thermal-conduction/
    thermal-conductivity/thermal-conductivity-of-uranium-dioxide/

    Args:
        T (float): Temperature in Kelvin

    Returns:
        float: Thermal conductivity in W/(cm K)

    """
    return 1 / (a + b * T + c * T**2) + d * np.exp(-e / T) / T ** (5 / 2)
