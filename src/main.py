from src.utils import thermal_cond
from src.solver import solve
import matplotlib.pyplot as plt
import numpy as np


def main():
    """
    Solve the heat equation in a cylinder of radius, r, 1 cm made of uranium dioxide
    considering a variable thermal conductivity, k, for this material, a power density,
    Egen, of 300 W/cm3 and an imposed condition of a temperature , Tsurface, of 800 °C
    at the outer perimeter.
    """
    # ----------------------#
    # Set variables
    # ----------------------#
    T_surface = 1073.15  # K
    Egen = 300  # W/(cm K)
    r_max = 1  # cm
    r_min = 0.001  # cm
    resolution = 100  # nb of points for discretisation

    # ----------------------#
    # Discretisation
    # ----------------------#
    r = np.linspace(r_min, r_max, resolution)

    # ----------------------#
    # Make guesses on :
    # - T
    # - dTdr
    # - k
    # ----------------------#
    pars_T = np.polyfit(
        [0, 0.4, 1], [1110, 1100, T_surface], 2
    )  # fit second degree polynomial
    T_guess = np.polyval(pars_T, r)

    pars_dTdr = np.polyfit([0, 1], [0, -40], 1)
    dTdr_guess = np.polyval(pars_dTdr, r)

    pars_k = np.polyfit(
        [0.3, 0.8, 1],
        [thermal_cond(1110), thermal_cond(1100), thermal_cond(T_surface)],
        2,
    )
    k_guess = np.polyval(pars_k, r)

    # ----------------------#
    # Solve
    # ----------------------#
    r_sol, T, dTdr, k = solve(r, [T_guess, dTdr_guess, k_guess], T_surface, Egen)

    # ----------------------#
    # Plot and save in
    # solution
    # ----------------------#
    fig, ax = plt.subplots(figsize=(10, 6))
    ax.plot(r_sol, T)

    ax.set_title(
        """Temperature profile from the center of \n
        the cylinder to its lateral surface with a variable thermal condictivity"""
    )
    ax.set_ylabel("Temperature K")
    ax.set_xlabel("Radius cm")

    plt.savefig(f"/home/jovyan/work/heat_eq/solution/T_profiles")


if __name__ == "__main__":
    main()
