"""
Contant values
"""
import numpy as np

a = 7.5408e-2
b = 1.7692e-4
c = 3.6142e-8
d = 6.400e9 * np.sqrt(1000)
e = 1.635e4
f = 2 * c
g = 1.60e8 * np.sqrt(1000)
h = 1.0464e12 * np.sqrt(1000)
