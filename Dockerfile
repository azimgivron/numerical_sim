FROM jupyter/scipy-notebook:latest

# Python
COPY requirements.txt /tmp/requirements.txt
RUN pip install -r /tmp/requirements.txt