# Numerical Simulation
## Problem statement

*Solve the heat equation in a cylinder of radius, r, 1 cm made of uranium dioxide considering a variable thermal conductivity, k, for this material, a power density, Egen, of 300 W/cm3 and an imposed condition of a temperature , Tsurface, of 800 °C at the outer perimeter.*

## Solution

The **heat equation in cylindrical coordinates**:

```math
\rho c \frac{\partial T}{\partial t} = \frac{1}{r} \frac{\partial}{\partial r} (k r \frac{\partial T}{\partial r}) + \frac{1}{r^2} \frac{\partial}{\partial \phi} (k \frac{\partial T}{\partial \phi}) + \frac{\partial}{\partial z} (k \frac{\partial T}{\partial z}) + E_{gen}
```

Assumptions are:
* Steady state.
```math
\frac{\partial }{\partial t} = 0
```
* The length of the cylinder is much greater than its radius. Hence, the heat is only dissipated through the lateral surface and none of it goes in the vertical direction(z).
```math
\frac{\partial T}{\partial z} = 0
```
* The heat diffusion is isotropic as the material is homogeneous.
```math
\frac{\partial T}{\partial \phi} = 0
```
* The thermal conductivity varies with the temperature according to the folliwng equation:
```math
k(T) = \frac{10^{2}}{7.5408 + 1.7692\times10^{-2} T + 3.6142\times10^{-6} T^2} + \frac{6.400\times10^7\times\sqrt{1000}}{T^{\frac{5}{2}}} \exp(\frac{-1.635\times10^4}{T})
```
* The melting point of Uranium Dioxyde is 2800°C.

This gives the following equation 
```math
\frac{1}{r} \frac{\partial}{\partial r} (k(T) r \frac{\partial T}{\partial r})  + E_{gen} = 0
```

**Note** : derivatives are not partial but ordinary, however I couldn't find the symbol for markdown. 

Using the derivative of a product rule, we get
```math
\frac{\partial k(T)}{\partial r}\frac{\partial T}{\partial r}  + k(T)\frac{\partial^2 T}{\partial^2 r} + \frac{k(T)}{r}\frac{\partial T}{\partial r} + E_{gen} = 0
```
Computing the derivative of the thermal conductivity with respect to r, we get:
```math
\begin{aligned}
\iff \frac{\partial k(T)}{\partial r} = (-10^{2}\times(\frac{1.7692\times10^{-2} + 7.2284\times10^{-6} T)}{(7.5408 + 1.7692\times10^{-2} T + 3.6142\times10^{-6} T^2)^2} \\[20pt]
										- \frac{1.60\times10^8\times\sqrt{1000}} {T^{\frac{7}{2}}} \exp(\frac{-1.635\times10^4} {T}) \\[20pt]
										+ \frac{1.0464\times10^{12}\times\sqrt{1000}}{T^{\frac{9}{2}}} \exp(\frac{-1.635\times10^4}{T})) \frac{\partial T}{\partial r}
										 
\end{aligned}
```
Therefore, we can write a **system of ordinary differential equations**:
```math
\frac{\partial T}{\partial r}  = D \\[20pt]
```
```math
\begin{aligned}
\frac{\partial k(T)}{\partial r} = (-\frac{10^{2}\times(1.7692\times10^{-2} + 7.2284\times10^{-6} T)}{(7.5408 + 1.7692\times10^{-2} T + 3.6142\times10^{-6} T^2)^2} \\[20pt]
									- \frac{1.60\times10^8\times\sqrt{1000}} {T^{\frac{7}{2}}} \exp(\frac{-1.635\times10^4} {T}) \\[20pt]
									+ \frac{1.0464\times10^{12}\times\sqrt{1000}}{T^{\frac{9}{2}}} \exp(\frac{-1.635\times10^4}{T})) D \\[20pt]
										 
\end{aligned}
```
```math
\frac{\partial D}{\partial r}  = - \frac{1}{k(T)} (E_{gen} + (\frac{\partial k(T)}{\partial r} + \frac{k(T)}{r}) D)
```
With the boundary conditions:
* Surface temperature
```math
T(r=r_{surface}=1cm) = 800°C
```
* Symmetry of the problem
```math
\frac{\partial T}{\partial r}(r=0cm) = 0
```
The obtained solution w.r.t. the radius:
![solution](solution/T_profiles.png)

In 3D:
![solution](solution/temp_profile3D.png)

## Running the code
### Requirements
* docker
* docker-compose
### Run
From your terminal, in the root folder, run: <br>
```
docker compose up
```
Once launched, a jupyterlab url will be shown. Paste it in your browser. From there, go inside 'work/heat_eq' and either run the 'src/main.py' file or go in the 'analysis' folder to play with the jupyter notebook.

Terminate with crt-C and then:
```
docker compose down
```

## Structure
* 'analysis' contains the analysis on the models and a jupyter notebook to play with the code interactively.
* 'src' contains the code.
* 'solution' and 'analysis/figures' contain figure of the results.
* 'test' is meant to contain unit tests but none was made.


